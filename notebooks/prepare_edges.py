# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python (network-visualization)
#     language: python
#     name: network_visualization
# ---

import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity

SKILLS = "data science"
N_SIMILAR_SKILLS = 250

embeddings = pd.read_pickle("../data/instaffo/embeddings.p")

similarities = cosine_similarity(
    embeddings.loc["data science"].values.reshape(1, -1), embeddings
)

similarities_data_science = pd.Series(similarities[0], index=embeddings.index)

similar_skills = (
    similarities_data_science.sort_values(ascending=False).head(N_SIMILAR_SKILLS).index
)

# +
similarities = cosine_similarity(embeddings.loc[similar_skills])
similarities = pd.DataFrame(similarities, index=similar_skills, columns=similar_skills)
similarities = similarities.reset_index().melt(id_vars="index")

similarities.columns = ["from", "to", "weight"]

similarities = similarities[similarities["from"] < similarities["to"]]

similarities = similarities.sort_values(["from", "to"])

similarities = similarities.reset_index(drop=True)
# -

similarities.head()

import numpy as np
import matplotlib.pyplot as plt

plt.hist(similarities["weight"])

plt.hist(similarities["weight"] ** 2.5)

similarities["weight"] = similarities["weight"] ** 2.5

similarities.to_pickle("../data/raw/edges.p")
