# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python (network-visualization)
#     language: python
#     name: network_visualization
# ---

# # Preparing the Nodes

import numpy as np
import pandas as pd

skills = pd.read_pickle("../data/instaffo/skills.p")

edges = pd.read_pickle("../data/raw/edges.p")

skills_index = np.unique(list(edges["from"]) + list(edges["to"]))

skills = skills[skills["name"].isin(skills_index)][["name", "frequency"]].reset_index(
    drop=True
)

skills["size"] = skills["frequency"] / skills["frequency"].max()

skills = skills[["name", "size"]]
skills.columns = ["id", "size"]

skills.to_pickle("../data/raw/nodes.p")
