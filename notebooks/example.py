# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python (network-visualization)
#     language: python
#     name: network_visualization
# ---

# # Example

import sys

sys.path.append("..")

import pandas as pd

from interactive_network_visualization.controllers import NetworkAnalyzer

nodes = pd.read_pickle("../data/raw/nodes.p").set_index("id")
edges = pd.read_pickle("../data/raw/edges.p").set_index(["from", "to"])

network_analyzer = NetworkAnalyzer(nodes, edges)

network_analyzer
