# interactive-network-visualization

Our colleague [Jan Jagusch](https://www.linkedin.com/in/janjagusch/)'s presentation about visualizing interactive graph networks in Python at the [PyCon DE & PyData Berlin](https://de.pycon.org/).

In this talk you will learn how to visualize graph networks in Python, using `networkx`, `traitlets`, `ipywidgets` and `plotly`. The resulting plot will be fully interactive, which makes it easy to filter edges, find nodes by their name and update the graph's layout.

![Example](resources/example.gif)

## Installation

### Prerequisites

This project requires and installation of `plotly` and `ipywidgets`. To make those packages work inside a Jupyter Lab session, you will need to install some extensions. We advise to:

- first follow the `ipywidgets` installation guide [here](https://ipywidgets.readthedocs.io/en/latest/user_install.html) and
- afterwards follow the `plotly` installation guide [here](https://github.com/plotly/plotly.py).

### Dependencies

For more information, please take a look at the `tool.poetry.dependencies` section in `pyproject.toml`.

### User Installation

You can check the latest source code with the command:

```
git clone git@gitlab.com:InstaffoOpenSource/DataScience/interactive-network-visualization.git
```

## Examples

```
>>> import pandas as pd
>>> from network_visualization.controllers import NetworkAnalyzer

>>> nodes = pd.read_pickle("data/raw/nodes.p").set_index("id")
>>> edges = pd.read_pickle("data/raw/edges.p").set_index(["from", "to"])

>>> network_analyzer = NetworkAnalyzer(nodes, edges)
>>> network_analyzer
```

For an interactive example, please take a look at [examples.py](notebooks/exaples.py). It can be converted to a `.ipynb` file, using [jupytext](https://github.com/mwouts/jupytext).

## Changelog

Please mind the [changelog](CHANGELOG.md) for notable changes to this project.

## License

See the [license](LICENSE) for details.

## Development

We welcome new contributions to this project!

### Dependencies

Please take a look at `tool.poetry.dev-dependencies` in `pyproject.toml`.

### Linting

After cloning and installing the dependencies, you can lint the project by executing:

```
make lint
```

### Testing

After cloning and installing the dependencies, you can test the project by executing:

```
make test
```

## Help and Support

### Authors

- Jan-Benedikt Jagusch <jan@instaffo.de>

## Acknowledgements

- This open-source contribution is made possible by [Instaffo](https://instaffo.com/). If you would like to know more about working as a developer in the HR industry, check our our [tech blog](https://www.instaffo.tech/).