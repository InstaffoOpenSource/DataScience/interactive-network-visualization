"""
Helpers for the tests.
"""
import json

import networkx as nx
import pandas as pd
import pytest


@pytest.fixture(name="nodes")
def nodes_():
    """
    Nodes.
    """
    return pd.read_json("tests/data/nodes.json").T


@pytest.fixture(name="edges")
def edges_():
    """
    Edges.
    """
    return pd.read_json("tests/data/edges.json").set_index(["from", "to"])


@pytest.fixture(name="graph")
def graph_(nodes, edges):
    """
    Graph.
    """
    graph = nx.Graph()
    graph.add_nodes_from((key, value) for key, value in nodes.to_dict("index").items())
    graph.add_edges_from(
        (key[0], key[1], value) for key, value in edges.to_dict("index").items()
    )
    return graph


@pytest.fixture(name="layout")
def layout_():
    """
    Layout.
    """
    with open("tests/data/layout.json") as file_pointer:
        return json.load(file_pointer)
