"""
Tests for the models.
"""
import numpy as np
import pandas as pd
import pytest

from interactive_network_visualization import models


@pytest.mark.parametrize(
    "data_frame,col,val_range,expected_val_ids",
    [
        (
            pd.DataFrame(
                {
                    "ind_a": {"col_a": -1},
                    "ind_b": {"col_a": -0},
                    "ind_c": {"col_a": 1},
                    "ind_d": {"col_a": 2},
                }
            ).T,
            "col_a",
            (0, 1),
            ["ind_b", "ind_c"],
        )
    ],
)
def test_range_filter(data_frame, col, val_range, expected_val_ids):
    range_filter = models.RangeFilter(data_frame, col)
    range_filter.val_range = val_range
    assert range_filter.val_ids == expected_val_ids


@pytest.mark.parametrize(
    "nodes_range,edges_range,expected_nodes_ids,expected_edges_ids",
    [
        (
            (0.01, 0.04),
            (0.4, 0.7),
            ["algorithmic trading", "alteryx"],
            [("algorithmic trading", "alteryx")],
        )
    ],
)
def test_nodes_edges_filter(
    nodes, edges, nodes_range, edges_range, expected_nodes_ids, expected_edges_ids
):
    nodes_edges_filter = models.NodesEdgesFilter(nodes, edges)
    nodes_edges_filter.nodes_range = nodes_range
    nodes_edges_filter.edges_range = edges_range

    assert nodes_edges_filter.nodes_ids == expected_nodes_ids
    assert nodes_edges_filter.edges_ids == expected_edges_ids


@pytest.mark.parametrize(
    "layout_method,k,iterations,threshold",
    [("fruchterman_reingold_layout", 0.1, 50, 0.0001)],
)
def test_graph_layout(graph, layout_method, k, iterations, threshold):
    print(threshold)
    graph_layout = models.GraphLayout(graph)
    graph_layout.layout_method = layout_method
    graph_layout.k = k
    graph_layout.iterations = iterations
    graph_layout.threshold = threshold

    graph_layout.update_layout()

    assert graph_layout.layout


@pytest.mark.parametrize(
    "node_id,expected_coords", [("alteryx", [0.75, 0.85, 0.75, 0.85])]
)
def test_node_focus(nodes, layout, node_id, expected_coords):
    node_focus = models.NodesFocus(nodes, layout)
    node_focus.node_id = node_id

    assert np.allclose(node_focus.coords, expected_coords)
